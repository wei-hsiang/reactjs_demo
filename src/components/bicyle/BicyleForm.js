import React from 'react';

export default class BicyleForm extends React.Component {
    constructor(props) {
        super(props);
        //初始化state
        this.state = {
            city: null,
            top: null,
            formErrors: {
                city: "",
                top: ""
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }
    
    //在每一次鍵盤被敲擊時都會被執行，並更新 React 的 state，
    //因此被顯示的 value 將會在使用者打字的同時被更新。
    handleInputChange = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value //透過[name]取得name的value並更新state value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        //表單驗證
        // if (formValid(this.state.formErrors)) {
        //     console.log(`
        //         "---SUBMITTING---"
        //         City: ${this.state.city}
        //         Top: ${this.state.top}
        //     `)
        // } else {
        //     console.error('FORM INVALID')
        // }
    }

    render() {
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <label> city: </label>
                                <input  
                                    name="city"
                                    type="text"
                                    value={this.state.city}
                                    onChange={this.handleInputChange} />
                            
                        </div>
                        <div>
                            <label> top : </label>
                                <input 
                                    name="top"
                                    type="number"
                                    value={this.state.top}
                                    onChange={this.handleInputChange} />
                        </div>
                        <div>
                            <button type="submit">search</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
