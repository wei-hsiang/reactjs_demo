import React from 'react';
import BicyleForm from './BicyleForm';
import BicycleFormFuntion from './BicycleFormFuntion';

class Bicyle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            item: [],
            isLoaded: false,
        }
        console.log('constructor');
    }

    //只要 function 標記為 async，就表示裡頭可以撰寫 await 的同步語法
    // async componentDidMount() {
    //     const url = "https://ptx.transportdata.tw/MOTC/v2/Bike/Availability/NewTaipei?$format=JSON";
    //     //await 會等待收到 resolve 之後才會進行後面的動作，如果沒有收到就會一直處在等待的狀態
    //     await fetch(url)
    //             .then(res => res.json())
    //             .then(json => {
    //                 this.setState({
    //                     isLoaded: true,
    //                     item: json,
    //                 })
    //             });
    // }

    render() {

        var { isLoaded, item } = this.state;

        if (!isLoaded) {
            return (
                <div> BicyleForm
                     <BicyleForm />
                     <br />
                     BicyleFormFuntion
                     <BicycleFormFuntion />
                </div>
            );
        } else {
            return (
                <div>
                    <h3>Bicyle Page</h3>
                    <ul>
                        {item.map(item => (
                            <li key={item.StationUID}>
                                可用車輛 : {item.AvailableRentBikes}
                            </li>
                        ))};
                    </ul>
                </div>
            );
        }

    }
}

export default Bicyle;
