import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const styles = () => ({
    card: {
        maxWidth: 420,
        marginTop: 50
    },
    container: {
        display: "Flex",
        justifyContent: "center"
    },
    actions: {
        float: "right"
    }
});

const courseCategory = [
    {
        value: "webDevelopment",
        label: "Web Development"
    },
    {
        value: "networking",
        label: "Networking"
    },
    {
        value: "computerScience",
        label: "Computer Science"
    }
];

const form = props => {
    const {
        classes,
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
        handleReset
    } = props;

    return (
        <div className={classes.container}>
            <form onSubmit={handleSubmit}>
                <Card className={classes.card}>
                    <CardContent>
                        <TextField
                            id="firstName"
                            label="First Name"
                            value={values.firstName}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.firstName ? errors.firstName : ""}
                            error={touched.firstName && Boolean(errors.firstName)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                        <TextField
                            id="lastName"
                            label="Last Name"
                            value={values.lastName}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.lastName ? errors.lastName : ""}
                            error={touched.lastName && Boolean(errors.lastName)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                        <TextField
                            id="email"
                            label="Email"
                            type="email"
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.email ? errors.email : ""}
                            error={touched.email && Boolean(errors.email)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                        <TextField
                            select
                            id="course"
                            label="Course Category"
                            value={values.course}
                            onChange={handleChange("course")}
                            helperText={touched.course ? errors.course : ""}
                            error={touched.course && Boolean(errors.course)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        >
                            {courseCategory.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                        <TextField
                            id="password"
                            label="Password"
                            type="password"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.password ? errors.password : ""}
                            error={touched.password && Boolean(errors.password)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                        <TextField
                            id="confirmPassword"
                            label="Confirm Password"
                            type="password"
                            value={values.confirmPassword}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched.confirmPassword ? errors.confirmPassword : ""}
                            error={touched.confirmPassword && Boolean(errors.confirmPassword)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                    </CardContent>
                    <CardActions className={classes.actions}>
                        <Button type="submit" color="primary" disabled={isSubmitting}>
                            SUBMIT
                        </Button>
                        <Button color="secondary" onClick={handleReset}>
                            CLEAR
                        </Button>
                    </CardActions>
                </Card>
            </form>
        </div>
    );
};

const LoginForm = withFormik({
    mapPropsToValues: ({
        firstName,
        lastName,
        email,
        course,
        password,
        confirmPassword
    }) => {
        return {
            firstName: firstName || "",
            lastName: lastName || "",
            email: email || "",
            course: course || "",
            password: password || "",
            confirmPassword: confirmPassword || ""
        };
    },
    // 表單驗證條件＆錯誤訊息
    validationSchema: Yup.object().shape({

        firstName: Yup.string()
            .required("Required"),
        lastName: Yup.string()
            .required("Required"),
        email: Yup.string()
            .email("Enter a valid email")
            .required("Email is required"),
        course: Yup.string()
            .required("Select your course category"),
        password: Yup.string()
            .min(8, "Password must contain at least 8 characters")
            .required("Enter your password"),
        confirmPassword: Yup.string()
            .required("Confirm your password")
            .oneOf([Yup.ref("password")], "Password does not match")
    }),
    // 點擊送出時
    handleSubmit: (values, { setSubmitting }) => {
        setTimeout(() => {
            // submit to the server
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 1000);

        fetch('/', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "firstName": this.firstName.value,
                "lastName": this.lastName.value,
                "email": this.email.value,
                "course": this.course.value,
                "password": this.password.value,
                "confirmPassword": this.confirmPassword.value
            })
        }).then((response) => {
            // return response.json();
            return "success";
        }).then((jsonData) => {
            console.log(jsonData);
        }).catch((err) => {
            console.log('錯誤:', err);
        });

    }
})(form);

export default withStyles(styles)(LoginForm);
